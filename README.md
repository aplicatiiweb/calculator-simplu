# Calculator Simplu

## Descriere
Acesta este un calculator simplu dezvoltat pentru a permite utilizatorilor să efectueze operații matematice de bază, precum adunare, scădere, înmulțire și împărțire. Interfața intuitivă și design-ul simplu facilitează utilizarea calculatorului, indiferent de nivelul de experiență în utilizarea tehnologiei.

## Caracteristici
- Interfață simplă și intuitivă
- Funcționalități de bază pentru operații matematice
- Design responsiv pentru compatibilitate cu diferite dispozitive
- Istoric al operațiilor pentru urmărirea rezultatelor anterioare
- Paletă de culori atractivă

## Utilizare
1. Introduceți numerele și operațiile folosind butoanele disponibile pe ecran.
2. Vizualizați rezultatul operației în câmpul de afișare.
3. Pentru a șterge întregul afișaj, apăsați butonul "C".

## Tehnologii utilizate
- HTML
- CSS
- JavaScript

## Cum să contribuiți
Dacă dorești să contribuiți la îmbunătățirea acestui calculator simplu, urmați acești pași:
1. Bifurcați acest depozit.
2. Faceți modificările necesare într-un branch separat.
3. Trimiteți un pull request pentru revizuire.

## Licență
Acest proiect este licențiat sub [Licența AGPL](https://www.gnu.org/licenses/agpl-3.0.html).
