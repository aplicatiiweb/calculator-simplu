function appendToDisplay(value) {
    document.getElementById('display').value += value;
}

function clearDisplay() {
    document.getElementById('display').value = '';
}

function calculate() {
    try {
        const expression = document.getElementById('display').value;
        // Validare a expresiei introduse
        if (/^[0-9()+\-*\/. ]+$/.test(expression)) {
            const result = eval(expression);
            document.getElementById('display').value = result;
            document.getElementById('history').innerHTML += expression + ' = ' + result + '<br>';
        } else {
            document.getElementById('display').value = 'Expresie invalidă!';
        }
    } catch (error) {
        document.getElementById('display').value = 'Error';
    }
}

// Limitarea numărului de caractere
document.getElementById('display').addEventListener('input', function() {
    if (this.value.length > 20) { // Limităm la maxim 20 de caractere
        this.value = this.value.slice(0, 20);
    }
});


